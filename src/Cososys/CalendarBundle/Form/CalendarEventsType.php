<?php

namespace Cososys\CalendarBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CalendarEventsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('eventFrom',null,['attr'=>['class'=>'timepicker','data-default-time'=>'false','placeholder'=>'h:mm PM']])
            ->add('calendarId','hidden')
            ->add('eventTo',null,['attr'=>['class'=>'timepicker','data-default-time'=>'false','placeholder'=>'h:mm PM']])
            ->add('eventDescription')
            ->add('eventTitle')
            ->add('calendar')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cososys\CalendarBundle\Entity\CalendarEvents'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cososys_calendarbundle_calendarevents';
    }
}
