<?php

namespace Cososys\CalendarBundle\Controller;



use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Cososys\CalendarBundle\Entity\Calendar;
use Cososys\CalendarBundle\Entity\CalendarEvents;
use Cososys\CalendarBundle\Entity\CalendarEventsRepository;
use Cososys\CalendarBundle\Entity\CalendarRepository;
use Cososys\CalendarBundle\Form\CalendarType;
use Cososys\CalendarBundle\Form\CalendarEventsType;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Calendar controller.
 *
 * @Route("/calendar")
 */
class CalendarController extends Controller
{

    /**
     * Lists all Calendar entities.
     *
     * @Route("/", name="calendar")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CososysCalendarBundle:Calendar')->findAllByOrderdByDate();

        $numberOfDays = cal_days_in_month(CAL_GREGORIAN,date('m'),date('Y'));

        return array(
            'entities' => $entities,
            'numberOfDays' => $numberOfDays,
            'currentMonth' => date('m'),
            'currentYear'  => date('Y')
        );
    }
    /**
     * Creates a new Calendar entity.
     *
     * @Route("/", name="calendar_create")
     * @Method("POST")
     * @Template("CososysCalendarBundle:Calendar:new.html.twig")
     */
    public function createAction(Request $request)
    {
        /* @var $date DateTime  */
        $entity = new Calendar();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $date = $form->getData()->getCalendarDate();
        $calendarDate = $date->format('Y-m-d');

        $em = $this->getDoctrine()->getManager();
        $existCalendar = $em->getRepository('CososysCalendarBundle:Calendar')->existsCalendarByDate($calendarDate);

        if(!$existCalendar){
            if ($form->isValid()) {
                $em->persist($entity);
                $em->flush();
                return $this->redirect($this->generateUrl('calendar'));
            }
        }else{
            $this->redirect('/calendar/new');
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Calendar entity.
     *
     * @param Calendar $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CalendarEvents $entity)
    {
        $form = $this->createForm(new CalendarEventsType(), $entity, array(
            'action' => $this->generateUrl('calendar_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }




    /**
     * Displays a form to create a new Calendar entity.
     *
     * @Route("/new", name="calendar_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Calendar();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Calendar entity.
     *
     * @Route("/{id}", name="calendar_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CososysCalendarBundle:Calendar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Calendar entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $allEntries = $em->getRepository('CososysCalendarBundle:CalendarEvents')->getAllEntriesByCalendarId($id);
        return array(
            'entity'      => $entity,
            'entries'     => $allEntries,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Calendar entity.
     *
     * @Route("/{id}/edit", name="calendar_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CososysCalendarBundle:Calendar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Calendar entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Calendar entity.
    *
    * @param Calendar $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Calendar $entity)
    {
        $form = $this->createForm(new CalendarType(), $entity, array(
            'action' => $this->generateUrl('calendar_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Calendar entity.
     *
     * @Route("/{id}", name="calendar_update")
     * @Method("PUT")
     * @Template("CososysCalendarBundle:Calendar:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CososysCalendarBundle:Calendar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Calendar entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('calendar_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Calendar entity.
     *
     * @Route("/{id}", name="calendar_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CososysCalendarBundle:Calendar')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Calendar entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('calendar'));
    }

    /**
     * Creates a form to delete a Calendar entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('calendar_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete Calendar'))
            ->getForm()
        ;
    }
    /**
     * Displays a form to create a new CalendarEvents entity.
     *
     * @Route("/events/new/{calendarDate}", name="new_calendar_event")
     * @Method("GET")
     * @Template()
     */
    public function newCalendarEventAction($calendarDate)
    {
        $entity = new CalendarEvents();
        $form   = $this->createCreateCalendarEventForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'calendarDate' => $calendarDate
        );
    }
    /**
     * Creates a form to create a CalendarEvents entity.
     *
     * @param CalendarEvents $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateCalendarEventForm(CalendarEvents $entity)
    {
        $form = $this->createForm(new CalendarEventsType(), $entity, array(
            'action' => $this->generateUrl('create_calendar_event'),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }
    /**
     * Creates a new CalendarEvents entity.
     *
     * @Route("/events/create", name="create_calendar_event")
     * @Method("POST")
     * @Template("CososysCalendarBundle:Calendar:newCalendarEvent.html.twig")
     */
    public function createCalendarEventAction(Request $request)
    {
        $entity = new CalendarEvents();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        $calendarDate = $request->get('calendar_date');
        $calendarId = $this->setCalendarEX($calendarDate);

        $eventFrom = $form->getData()->getEventFrom();
        $eventTo   = $form->getData()->getEventTo();

        $overlapping =  $em->getRepository('CososysCalendarBundle:CalendarEvents')->validateEventOverlapping($eventFrom,$eventTo,$calendarId);

        if(!$overlapping){
            return $this->redirect($this->generateUrl('calendar'));
        }

        $entity->setCalendarId($calendarId);
        if ($form->isValid()) {

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('calendar'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    private function setCalendarEX($calendarDate)
    {
        $calendar = new Calendar();
        $em = $this->getDoctrine()->getManager();

        $calendarId = $em->getRepository('CososysCalendarBundle:Calendar')->getCalendarByDate($calendarDate);
        if(empty($calendarId)){
            $calendar->setCalendarDate(new \DateTime($calendarDate));
            $em->persist($calendar);
            $em->flush();
            $calendarId = $calendar->getCalendarId();
        }else{
            $calendarId = $calendarId[0]->getCalendarId();
        }
        return  $calendarId;
    }

    /**
     * Finds and displays a CalendarEvents entity.
     *
     * @Route("events/show/{id}", name="show_calendar_event")
     * @Method("GET")
     * @Template()
     */
    public function showCalendarEventAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CososysCalendarBundle:CalendarEvents')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CalendarEvents entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Displays a form to edit an existing CalendarEvents entity.
     *
     * @Route("/events/{id}/edit", name="edit_calendar_event")
     * @Method("GET")
     * @Template()
     */
    public function editCalendarEventAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CososysCalendarBundle:CalendarEvents')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CalendarEvents entity.');
        }

        $editForm = $this->createCalendarEventEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing CalendarEvents entity.
     *
     * @Route("events/update/{id}", name="update_calendar_event")
     * @Method("PUT")
     * @Template("CososysCalendarBundle:CalendarEvents:updateCalendarEventAction.html.twig")
     */
    public function updateCalendarEventAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CososysCalendarBundle:CalendarEvents')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CalendarEvents entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createCalendarEventEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('calendarevents_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Creates a form to edit a CalendarEvents entity.
     *
     * @param CalendarEvents $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCalendarEventEditForm(CalendarEvents $entity)
    {
        $form = $this->createForm(new CalendarEventsType(), $entity, array(
            'action' => $this->generateUrl('calendarevents_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Deletes a CalendarEvents entity.
     *
     * @Route("events/{id}", name="delete_calendar_event")
     * @Method("DELETE")
     */
    public function deleteCalendarEventAction(Request $request, $id)
    {
        $form = $this->createCalendarEventDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CososysCalendarBundle:CalendarEvents')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CalendarEvents entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('calendarevents'));
    }
    private function createCalendarEventDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('calendarevents_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }
}
