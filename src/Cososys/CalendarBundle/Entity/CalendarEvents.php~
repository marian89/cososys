<?php

namespace Cososys\CalendarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Index;
/**
 * CalendarEvents
 *
 * @ORM\Table(name="calendar_event" ,indexes={
 *      @Index(name="event_to_idx", columns={"event_to"}),
 *      @Index(name="event_from_idx", columns={"event_from"}),
 * })
 * @ORM\Entity(repositoryClass="Cososys\CalendarBundle\Entity\CalendarEventsRepository")
 */
class CalendarEvents
{

    /**
     * @var  Calendar
     * @ORM\ManyToOne(targetEntity="Calendar", inversedBy="calendar_event")
     * @ORM\JoinColumn(name="calendar_id", referencedColumnName="calendar_id")
     */
    private $calendar;



    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="event_from", type="string")
     */
    private $eventFrom;

    /**
     * @var integer
     * @ORM\Column(name="calendar_id", type="integer")
     */
    private $calendarId;


    /**
     * @var string
     *
     * @ORM\Column(name="event_to", type="string")
     */

    private $eventTo;

    /**
     * @var string
     *
     * @ORM\Column(name="event_description", type="text")
     */
    private $eventDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="event_title", type="string", length=255)
     */
    private $eventTitle;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eventFrom
     *
     * @param \DateTime $eventFrom
     * @return CalendarEvents
     */
    public function setEventFrom($eventFrom)
    {
        $this->eventFrom = $eventFrom;

        return $this;
    }

    /**
     * Get eventFrom
     *
     * @return \DateTime 
     */
    public function getEventFrom()
    {
        return $this->eventFrom;
    }

    /**
     * Set eventTo
     *
     * @param \DateTime $eventTo
     * @return CalendarEvents
     */
    public function setEventTo($eventTo)
    {
        $this->eventTo = $eventTo;

        return $this;
    }

    /**
     * Get eventTo
     *
     * @return \DateTime 
     */
    public function getEventTo()
    {
        return $this->eventTo;
    }

    /**
     * Set eventDescription
     *
     * @param string $eventDescription
     * @return CalendarEvents
     */
    public function setEventDescription($eventDescription)
    {
        $this->eventDescription = $eventDescription;

        return $this;
    }

    /**
     * Get eventDescription
     *
     * @return string 
     */
    public function getEventDescription()
    {
        return $this->eventDescription;
    }

    /**
     * Set eventTitle
     *
     * @param string $eventTitle
     * @return CalendarEvents
     */
    public function setEventTitle($eventTitle)
    {
        $this->eventTitle = $eventTitle;

        return $this;
    }

    /**
     * Get eventTitle
     *
     * @return string 
     */
    public function getEventTitle()
    {
        return $this->eventTitle;
    }

    /**
     * Set calendarId
     *
     * @param integer $calendarId
     * @return CalendarEvents
     */
    public function setCalendarId($calendarId)
    {
        $this->calendarId = $calendarId;

        return $this;
    }

    /**
     * Get calendarId
     *
     * @return integer 
     */
    public function getCalendarId()
    {
        return $this->calendarId;
    }

    /**
     * Set calendar
     *
     * @param \Cososys\CalendarBundle\Entity\Calendar $calendar
     * @return CalendarEvents
     */
    public function setCalendar(\Cososys\CalendarBundle\Entity\Calendar $calendar = null)
    {
        $this->calendar = $calendar;

        return $this;
    }

    /**
     * Get calendar
     *
     * @return \Cososys\CalendarBundle\Entity\Calendar 
     */
    public function getCalendar()
    {
        return $this->calendar;
    }
}
