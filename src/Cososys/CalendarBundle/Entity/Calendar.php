<?php

namespace Cososys\CalendarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Calendar
 *
 * @ORM\Table(name="calendar")
 * @ORM\Entity(repositoryClass="Cososys\CalendarBundle\Entity\CalendarRepository")
 * 
 */
class Calendar
{
    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="CalendarEvents", mappedBy="calendar")
     *
     */
    private $calendar_event;

    /**
     * @var integer
     *
     * @ORM\Column(name="calendar_id", type="integer")
     *  @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $calendarId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="calendar_date", type="date")
     */
    private $calendarDate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->calendarId;
    }

    /**
     * Set calendarId
     *
     * @param integer $calendarId
     * @return Calendar
     */
    public function setCalendarId($calendarId)
    {
        $this->calendarId = $calendarId;

        return $this;
    }

    /**
     * Get calendarId
     *
     * @return integer 
     */
    public function getCalendarId()
    {
        return $this->calendarId;
    }

    /**
     * Set calendarDate
     *
     * @param \DateTime $calendarDate
     * @return Calendar
     */
    public function setCalendarDate($calendarDate)
    {
        $this->calendarDate = $calendarDate;

        return $this;
    }

    /**
     * Get calendarDate
     *
     * @return \DateTime
     */
    public function getCalendarDate()
    {
        return $this->calendarDate;
    }

    public function __construct()
    {
        $this->calendar_event = new ArrayCollection();
    }

    /**
     * Add calendar_event
     *
     * @param \Cososys\CalendarBundle\Entity\CalendarEvents $calendarEvent
     * @return Calendar
     */
    public function addCalendarEvent(\Cososys\CalendarBundle\Entity\CalendarEvents $calendarEvent)
    {
        $this->calendar_event[] = $calendarEvent;

        return $this;
    }

    /**
     * Remove calendar_event
     *
     * @param \Cososys\CalendarBundle\Entity\CalendarEvents $calendarEvent
     */
    public function removeCalendarEvent(\Cososys\CalendarBundle\Entity\CalendarEvents $calendarEvent)
    {
        $this->calendar_event->removeElement($calendarEvent);
    }

    /**
     * Get calendar_event
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCalendarEvent()
    {
        return $this->calendar_event;
    }

    public function existsCalendarByDate($date)
    {
        /* @var $entity Calendar */

    }
    public function __toString()
    {
        return 'aaa';
    }
}
